var argscheck = require('cordova/argscheck'),
    utils = require('cordova/utils'),
    exec = require('cordova/exec');

var PLUGIN_NAME = "DetectorPreview";

var DetectorPreview = function() {};

function isFunction(obj) {
    return !!(obj && obj.constructor && obj.call && obj.apply);
};

DetectorPreview.setDetectorView = function(onSuccess, onError) {
    exec(onSuccess, onError, PLUGIN_NAME, "setDetectorView", []);
};

DetectorPreview.stopDetector = function(onSuccess, onError) {
    exec(onSuccess, onError, PLUGIN_NAME, "stopDetector", []);
};

DetectorPreview.startDetector = function(onSuccess, onError) {
    exec(onSuccess, onError, PLUGIN_NAME, "startDetector", []);
};

DetectorPreview.FOCUS_MODE = {
    FIXED: 'fixed',
    AUTO: 'auto',
    CONTINUOUS: 'continuous', // IOS Only
    CONTINUOUS_PICTURE: 'continuous-picture', // Android Only
    CONTINUOUS_VIDEO: 'continuous-video', // Android Only
    EDOF: 'edof', // Android Only
    INFINITY: 'infinity', // Android Only
    MACRO: 'macro' // Android Only
};

DetectorPreview.EXPOSURE_MODE = {
    LOCK: 'lock',
    AUTO: 'auto', // IOS Only
    CONTINUOUS: 'continuous', // IOS Only
    CUSTOM: 'custom' // IOS Only
};

DetectorPreview.WHITE_BALANCE_MODE = {
    LOCK: 'lock',
    AUTO: 'auto',
    CONTINUOUS: 'continuous',
    INCANDESCENT: 'incandescent',
    CLOUDY_DAYLIGHT: 'cloudy-daylight',
    DAYLIGHT: 'daylight',
    FLUORESCENT: 'fluorescent',
    SHADE: 'shade',
    TWILIGHT: 'twilight',
    WARM_FLUORESCENT: 'warm-fluorescent'
};

DetectorPreview.FLASH_MODE = {
    OFF: 'off',
    ON: 'on',
    AUTO: 'auto',
    RED_EYE: 'red-eye', // Android Only
    TORCH: 'torch'
};

DetectorPreview.COLOR_EFFECT = {
    AQUA: 'aqua', // Android Only
    BLACKBOARD: 'blackboard', // Android Only
    MONO: 'mono',
    NEGATIVE: 'negative',
    NONE: 'none',
    POSTERIZE: 'posterize',
    SEPIA: 'sepia',
    SOLARIZE: 'solarize', // Android Only
    WHITEBOARD: 'whiteboard' // Android Only
};

DetectorPreview.CAMERA_DIRECTION = {
    BACK: 'back',
    FRONT: 'front'
};

module.exports = DetectorPreview;
