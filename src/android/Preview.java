package com.cordovaplugincamerapreview;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.RelativeLayout;
import org.apache.cordova.LOG;

import java.util.List;

class Preview extends RelativeLayout implements SurfaceHolder.Callback {
  private final String TAG = "Preview";

  CustomSurfaceView mSurfaceView;
  SurfaceHolder mHolder;
  Camera.Size mPreviewSize;
  List<Camera.Size> mSupportedPreviewSizes;
  int displayOrientation;
  DetectorPreview mDetectorPreview;

  Preview(Activity activity, DetectorPreview detectorPreview) {
    super((Context)activity);
    mDetectorPreview = detectorPreview;
    mSurfaceView = new CustomSurfaceView(activity);
    addView(mSurfaceView);

    requestLayout();

    // Install a SurfaceHolder.Callback so we get notified when the
    // underlying surface is created and destroyed.
    mHolder = mSurfaceView.getHolder();
    mHolder.addCallback(this);
    mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  }

  @Override
  protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
    // We purposely disregard child measurements because act as a
    // wrapper to a SurfaceView that centers the camera preview instead
    // of stretching it.
//    final int width = resolveSize(getSuggestedMinimumWidth(), widthMeasureSpec);
//    final int height = resolveSize(getSuggestedMinimumHeight(), heightMeasureSpec);
    final int width = 100;
    final int height = 100;
    setMeasuredDimension(width, height);

    if (mSupportedPreviewSizes != null) {
      mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, width, height);
    }
  }

  @Override
  protected void onLayout(boolean changed, int l, int t, int r, int b) {

    if (changed && getChildCount() > 0) {
      final View child = getChildAt(0);

      int width = r - l;
      int height = b - t;

      int previewWidth = width;
      int previewHeight = height;

      if (mPreviewSize != null) {
        previewWidth = mPreviewSize.width;
        previewHeight = mPreviewSize.height;

        if(displayOrientation == 90 || displayOrientation == 270) {
          previewWidth = mPreviewSize.height;
          previewHeight = mPreviewSize.width;
        }

        LOG.d(TAG, "previewWidth:" + previewWidth + " previewHeight:" + previewHeight);
      }

      int nW;
      int nH;
      int top;
      int left;

      float scale = 1.0f;

      // Center the child SurfaceView within the parent.
      if (width * previewHeight < height * previewWidth) {
        Log.d(TAG, "center horizontally");
        int scaledChildWidth = (int)((previewWidth * height / previewHeight) * scale);
        nW = (width + scaledChildWidth) / 2;
        nH = (int)(height * scale);
        top = 0;
        left = (width - scaledChildWidth) / 2;
      } else {
        Log.d(TAG, "center vertically");
        int scaledChildHeight = (int) ((previewHeight * width / previewWidth) * scale);
        nW = (int) (width * scale);
        nH = (height + scaledChildHeight) / 2;
        top = (height - scaledChildHeight) / 2;
        left = 0;
      }
      child.layout(left, top, nW, nH);

      Log.d("layout", "left:" + left);
      Log.d("layout", "top:" + top);
      Log.d("layout", "right:" + nW);
      Log.d("layout", "bottom:" + nH);
    }
  }

  public void surfaceCreated(SurfaceHolder holder) {
    // The Surface has been created, acquire the camera and tell it where
    // to draw.
    mDetectorPreview.setSurfaceView(mSurfaceView);
  }

  public void surfaceDestroyed(SurfaceHolder holder) {
    // Surface will be destroyed when we return, so stop the preview.
    try {
    } catch (Exception exception) {
      Log.e(TAG, "Exception caused by surfaceDestroyed()", exception);
    }
  }

  private Camera.Size getOptimalPreviewSize(List<Camera.Size> sizes, int w, int h) {
    final double ASPECT_TOLERANCE = 0.1;
    double targetRatio = (double) w / h;
    if (displayOrientation == 90 || displayOrientation == 270) {
      targetRatio = (double) h / w;
    }

    if(sizes == null){
      return null;
    }

    Camera.Size optimalSize = null;
    double minDiff = Double.MAX_VALUE;

    int targetHeight = h;

    // Try to find an size match aspect ratio and size
    for (Camera.Size size : sizes) {
      double ratio = (double) size.width / size.height;
      if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE) continue;
      if (Math.abs(size.height - targetHeight) < minDiff) {
        optimalSize = size;
        minDiff = Math.abs(size.height - targetHeight);
      }
    }

    // Cannot find the one match the aspect ratio, ignore the requirement
    if (optimalSize == null) {
      minDiff = Double.MAX_VALUE;
      for (Camera.Size size : sizes) {
        if (Math.abs(size.height - targetHeight) < minDiff) {
          optimalSize = size;
          minDiff = Math.abs(size.height - targetHeight);
        }
      }
    }

    Log.d(TAG, "optimal preview size: w: " + optimalSize.width + " h: " + optimalSize.height);
    return optimalSize;
  }

  public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
      requestLayout();
  }
}
