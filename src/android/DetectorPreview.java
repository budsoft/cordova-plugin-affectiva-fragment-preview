package com.cordovaplugincamerapreview;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.SurfaceView;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;

import java.util.List;

public class DetectorPreview extends CordovaPlugin implements Detector.ImageListener, CameraDetector.CameraEventListener {

  private static final String TAG = "DetectorPreview";

  private static final String START_DETECTOR = "startDetector";
  private static final String STOP_DETECTOR = "stopDetector";
  private static final String SET_DETECTOR_VIEW = "setDetectorView";

  private CameraActivity fragment;
  private CallbackContext detectorCallbackContext;
  public CameraDetector mDetector;
  public static CordovaInterface mCordova;
  public SurfaceView mSurfaceView;
  public FrameLayout containerView;

  private int containerViewId = 1;
  public DetectorPreview(){
    super();
    Log.d(TAG, "Constructing");
  }

  public void initialize(CordovaInterface cordova, CordovaWebView webView) {
    mCordova = cordova;
    super.initialize(mCordova, webView);
  }

  @Override
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    if (START_DETECTOR.equals(action)) {
        return this.startDetector(callbackContext);
    } else if (STOP_DETECTOR.equals(action)) {
      return this.stopDetector(callbackContext);
    } else if (SET_DETECTOR_VIEW.equals(action)) {
      return this.setDetectorView(0,60,200,200, callbackContext);
    }
    return false;
  }

  public void initDetector(final SurfaceView mSurfaceView) {
    mCordova.getActivity().runOnUiThread(new Runnable() {
       @Override
       public void run() {
//         mCordova.getActivity().setContentView(mSurfaceView);
         FrameLayout containerView = (FrameLayout)mCordova.getActivity().findViewById(containerViewId);
         if(mSurfaceView.getParent()!=null){
           ((ViewGroup)mSurfaceView.getParent()).removeView(mSurfaceView); // <- fix
         }
         containerView.addView(mSurfaceView); //  <==========  ERROR IN THIS LINE DURING 2ND RUN
         setDetector();
       }
     });
  }

  public void setDetector(){
    CameraDetector detector = new CameraDetector(mCordova.getActivity().getApplicationContext(), CameraDetector.CameraType.CAMERA_FRONT, mSurfaceView);
    detector.setDetectAllAppearances(true);
    detector.setDetectAllEmojis(true);
    detector.setDetectAllEmotions(true);
    detector.setDetectAllExpressions(true);
    detector.setImageListener(this);
    detector.setOnCameraEventListener(this);
    mDetector = detector;
  }

  public boolean setDetectorView(int x, int y, int width, int height, CallbackContext callbackContext){
    fragment = new CameraActivity();
    fragment.mDetectorPreview = this;

    DisplayMetrics metrics = mCordova.getActivity().getResources().getDisplayMetrics();
    // offset
    int computedX = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, x, metrics);
    int computedY = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, y, metrics);

    // size
    int computedWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, metrics);
    int computedHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, metrics);

    fragment.setRect(computedX, computedY, computedWidth, computedHeight);

    detectorCallbackContext = callbackContext;

    mCordova.getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {

        //create or update the layout params for the container view
        FrameLayout containerView = (FrameLayout)mCordova.getActivity().findViewById(containerViewId);
        if(containerView == null){
          containerView = new FrameLayout(mCordova.getActivity().getApplicationContext());
          containerView.setId(containerViewId);

          FrameLayout.LayoutParams containerLayoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
          mCordova.getActivity().addContentView(containerView, containerLayoutParams);
        }
        //display camera bellow the webview

//          webView.getView().setBackgroundColor(0x00000000);
//          ((ViewGroup)webView.getView()).bringToFront();

        //set camera back to front
//          containerView.setAlpha(opacity);
        containerView.bringToFront();
        //add the fragment to the container
        FragmentManager fragmentManager = mCordova.getActivity().getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(containerView.getId(), fragment);
        fragmentTransaction.commit();
      }
    });
    return true;
  }

  public boolean startDetector(CallbackContext callbackContext) {
    Log.d(TAG, "start detector action");
    if(mDetector == null){
      this.initDetector(mSurfaceView);
    }
    mCordova.getActivity().runOnUiThread(new Runnable() {
      @Override
      public void run() {
        if (!mDetector.isRunning()) {
          mDetector.start();
        }

      }
    });

    return true;
  }

  public void setSurfaceView(SurfaceView surfaceView) {
    this.mSurfaceView = surfaceView;
  }

  public boolean stopDetector(CallbackContext callbackContext) {
    if (mDetector.isRunning()) {
      mCordova.getActivity().runOnUiThread(new Runnable() {

        public void run() {
          mDetector.stop();

          FragmentManager fragmentManager = mCordova.getActivity().getFragmentManager();
          FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
          fragmentTransaction.remove(fragment);
          fragmentTransaction.commit();
          fragment = null;
        }
      });
    }
    return true;
  }

  @Override
  public void onImageResults(List<Face> faces, Frame receievedFrame, float v) {
    final List<Face> list = faces;
    final Frame frame = receievedFrame;
    mCordova.getThreadPool().execute(new Runnable() {
      public void run() {
        try {

          if (list == null){
            PluginResult progressResult = new PluginResult(PluginResult.Status.ERROR, "NO FACE");
            progressResult.setKeepCallback(true);
            detectorCallbackContext.sendPluginResult(progressResult);
          }
          if (list.size() == 0) {
            PluginResult progressResult = new PluginResult(PluginResult.Status.ERROR, "NO FACE");
            progressResult.setKeepCallback(true);
            detectorCallbackContext.sendPluginResult(progressResult);
          }
          else {
            //HAS FACES RESULTS, PARSE TO JSON AND "callback.sendPluginResult"
              JSONObject jsonObj = new JSONObject();
            JSONObject jsonObjQualities = new JSONObject();
            JSONObject jsonObjAppearance = new JSONObject();
            JSONObject jsonObjEmojis = new JSONObject();
            JSONObject jsonObjExpressions = new JSONObject();
            JSONObject jsonObjEmotions = new JSONObject();
            JSONObject jsonObjMeasurements = new JSONObject();
            JSONObject jsonObjFrame = new JSONObject();

            try {
              //appearance
              jsonObjAppearance.put("Age", list.get(0).appearance.getAge().toString());
              jsonObjAppearance.put("Ethnicity", list.get(0).appearance.getEthnicity().toString());
              jsonObjAppearance.put("Gender", list.get(0).appearance.getGender().toString());
              jsonObjAppearance.put("Glasses", list.get(0).appearance.getGlasses().toString());

              //qualities
              jsonObjQualities.put("Brightness", String.valueOf(list.get(0).qualities.getBrightness()));

              //emojis
              jsonObjEmojis.put("Disappointed", String.valueOf(list.get(0).emojis.getDisappointed()));
              jsonObjEmojis.put("Dominant", String.valueOf(list.get(0).emojis.getDominant()));
              jsonObjEmojis.put("DominantEmoji", String.valueOf(list.get(0).emojis.getDominantEmoji()));
              jsonObjEmojis.put("Flushed", String.valueOf(list.get(0).emojis.getFlushed()));
              jsonObjEmojis.put("getKissing", String.valueOf(list.get(0).emojis.getKissing()));
              jsonObjEmojis.put("Laughing", String.valueOf(list.get(0).emojis.getLaughing()));
              jsonObjEmojis.put("Rage", String.valueOf(list.get(0).emojis.getRage()));
              jsonObjEmojis.put("Relaxed", String.valueOf(list.get(0).emojis.getRelaxed()));
              jsonObjEmojis.put("Scream", String.valueOf(list.get(0).emojis.getScream()));
              jsonObjEmojis.put("Smiley", String.valueOf(list.get(0).emojis.getSmiley()));
              jsonObjEmojis.put("Smirk", String.valueOf(list.get(0).emojis.getSmirk()));
              jsonObjEmojis.put("StuckOutTongue", String.valueOf(list.get(0).emojis.getStuckOutTongue()));
              jsonObjEmojis.put("StuckOutTongueWinkingEye", String.valueOf(list.get(0).emojis.getStuckOutTongueWinkingEye()));
              jsonObjEmojis.put("Wink", String.valueOf(list.get(0).emojis.getWink()));

              //expressions
              jsonObjExpressions.put("Attention", String.valueOf(list.get(0).expressions.getAttention()));
              jsonObjExpressions.put("Smirk", String.valueOf(list.get(0).expressions.getSmirk()));
              jsonObjExpressions.put("BrowFurrow", String.valueOf(list.get(0).expressions.getBrowFurrow()));
              jsonObjExpressions.put("BrowRaise", String.valueOf(list.get(0).expressions.getBrowRaise()));
              jsonObjExpressions.put("CheekRaise", String.valueOf(list.get(0).expressions.getCheekRaise()));
              jsonObjExpressions.put("ChinRaise", String.valueOf(list.get(0).expressions.getChinRaise()));
              jsonObjExpressions.put("Dimpler", String.valueOf(list.get(0).expressions.getDimpler()));
              jsonObjExpressions.put("EyeClosure", String.valueOf(list.get(0).expressions.getEyeClosure()));
              jsonObjExpressions.put("EyeWiden", String.valueOf(list.get(0).expressions.getEyeWiden()));
              jsonObjExpressions.put("InnerBrowRaise", String.valueOf(list.get(0).expressions.getInnerBrowRaise()));
              jsonObjExpressions.put("JawDrop", String.valueOf(list.get(0).expressions.getJawDrop()));
              jsonObjExpressions.put("LidTighten", String.valueOf(list.get(0).expressions.getLidTighten()));
              jsonObjExpressions.put("LipCornerDepressor", String.valueOf(list.get(0).expressions.getLipCornerDepressor()));
              jsonObjExpressions.put("LipPress", String.valueOf(list.get(0).expressions.getLipPress()));
              jsonObjExpressions.put("LipCornerDepressor", String.valueOf(list.get(0).expressions.getLipCornerDepressor()));
              jsonObjExpressions.put("LidTighten", String.valueOf(list.get(0).expressions.getLidTighten()));
              jsonObjExpressions.put("LipPucker", String.valueOf(list.get(0).expressions.getLipPucker()));
              jsonObjExpressions.put("LipStretch", String.valueOf(list.get(0).expressions.getLipStretch()));
              jsonObjExpressions.put("LipSuck", String.valueOf(list.get(0).expressions.getLipSuck()));
              jsonObjExpressions.put("NoseWrinkle", String.valueOf(list.get(0).expressions.getNoseWrinkle()));
              jsonObjExpressions.put("Smile", String.valueOf(list.get(0).expressions.getSmile()));
              jsonObjExpressions.put("Smirk", String.valueOf(list.get(0).expressions.getSmirk()));
              jsonObjExpressions.put("UpperLipRaise", String.valueOf(list.get(0).expressions.getUpperLipRaise()));
              jsonObjExpressions.put("Smile", String.valueOf(list.get(0).expressions.getSmile()));

              //emotions
              jsonObjEmotions.put("Anger", String.valueOf(list.get(0).emotions.getAnger()));
              jsonObjEmotions.put("Contempt", String.valueOf(list.get(0).emotions.getContempt()));
              jsonObjEmotions.put("Disgust", String.valueOf(list.get(0).emotions.getDisgust()));
              jsonObjEmotions.put("Engagement", String.valueOf(list.get(0).emotions.getEngagement()));
              jsonObjEmotions.put("Fear", String.valueOf(list.get(0).emotions.getFear()));
              jsonObjEmotions.put("Joy", String.valueOf(list.get(0).emotions.getJoy()));
              jsonObjEmotions.put("Sadness", String.valueOf(list.get(0).emotions.getSadness()));
              jsonObjEmotions.put("Surprise", String.valueOf(list.get(0).emotions.getSurprise()));
              jsonObjEmotions.put("Valence", String.valueOf(list.get(0).emotions.getValence()));

              //measurements
              jsonObjMeasurements.put("InterocularDistance", String.valueOf(list.get(0).measurements.getInterocularDistance()));

              //frame
              jsonObjFrame.put("ColorFormat", frame.getColorFormat().toString());
              jsonObjFrame.put("Height", frame.getHeight());
              jsonObjFrame.put("PixelCount", frame.getPixelCount());
              jsonObjFrame.put("TargetRotation", frame.getTargetRotation().toString());
              jsonObjFrame.put("Width", frame.getWidth());
              jsonObjFrame.put("Height", frame.getHeight());

              //global
              jsonObj.put("appearance", jsonObjAppearance);
              jsonObj.put("qualities", jsonObjQualities);
              jsonObj.put("emojis", jsonObjEmojis);
              jsonObj.put("expressions", jsonObjExpressions);
              jsonObj.put("emotions", jsonObjEmotions);
              jsonObj.put("measurements", jsonObjMeasurements);
              jsonObj.put("facePoints", list.get(0).getFacePoints());
              jsonObj.put("frame", jsonObjFrame);
              jsonObj.put("id", list.get(0).getId());

            } catch (JSONException e) {
              e.printStackTrace();
            }

            PluginResult progressResult = new PluginResult(PluginResult.Status.OK, jsonObj);
            progressResult.setKeepCallback(true);
            detectorCallbackContext.sendPluginResult(progressResult);

          }
        } catch (Throwable e) {
          detectorCallbackContext.error(e.toString());
        }
      }
    });
  }

  @SuppressWarnings("SuspiciousNameCombination")
  @Override
  public void onCameraSizeSelected(int width, int height, Frame.ROTATE rotate) {
    mSurfaceView.requestLayout();
  }

}
